FROM alpine:3.17.0

RUN apk add --no-cache nginx

RUN addgroup -S www && \
  adduser -D -S -h /www -s /sbin/nologin -G www www

COPY files/nginx.conf /etc/nginx/nginx.conf
COPY files/practest.html /www/practest.html

EXPOSE 8081

CMD ["nginx"]
